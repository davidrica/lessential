let elixir = require('laravel-elixir');
require('laravel-elixir-livereload');

elixir.config.assetsPath = 'themes/lessential/assets/';
elixir.config.publicPath = 'themes/lessential/assets/compiled/';

elixir(function(mix){

    mix.sass('style.scss');

    mix.scripts([
        'jquery.js',
        'uikit.js',
        'uikit-icons.js',
        'semantic/transition.min.js',
        'semantic/popup.min.js',
        'slick.js',
        'app.js'
    ]);

    mix.livereload([
        'themes/lessential/assets/compiled/css/style.css',
        'themes/lessential/**/*.htm',
        'themes/lessential/assets/compiled/js/*.js'
    ])

});
