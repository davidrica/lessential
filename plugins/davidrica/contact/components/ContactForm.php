<?php namespace Davidrica\Contact\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;

class ContactForm extends ComponentBase
{

	public function componentDetails(){
		return [
			'name' => 'Contact Form',
			'description' => 'Simple contact form'
		];
	}


	public function onSend(){

		$vars = ['name' => Input::get('name'), 'email' => Input::get('email'), 'txtMessage' => Input::get('txtMessage')];

		Mail::send('davidrica.contact::mail.message', $vars, function($message) {

			$message->from('sender@reviewhotel.co.id', 'reviewhotel.co.id');
		    $message->to('contact@reviewhotel.co.id', 'Admin Person');
		    $message->subject('New message from reviewhotel.co.id');

		});

	}
}