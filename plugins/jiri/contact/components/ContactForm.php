<?php namespace Jiri\Contact\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;

class ContactForm extends ComponentBase
{

	public function componentDetails(){
		return [
			'name' => 'Contact Form',
			'description' => 'Simple contact form'
		];
	}

    public function defineProperties()
    {
        return [
            'idElementMessageStatus' => [
                'title'       => 'ID Element Message Status',
                'description' => 'This Element will be refresh after success send message',
                'default'     => '#message-status',
                'type'        => 'string'
            ]
        ];
    }


	public function onSend(){

		$vars = ['name' => Input::get('name'), 'email' => Input::get('email'),  'subject' => Input::get('subject'), 'phone' => Input::get('phone'), 'txtMessage' => Input::get('txtMessage')];

		Mail::send('jiri.contact::mail.message', $vars, function($message) {

			$message->from('sender@lessentialbeauty.id', 'lessentialbeauty.id');
		    $message->to('davidlikaldo@gmail.com', 'Lessential Beauty');
		    $message->subject('New message from lessentialbeauty.id');

		});

        return [
            $this->property("idElementMessageStatus") => $this->renderPartial('::message-status')
        ];

	}
}
