<?php namespace Jiri\JKShop\Components;

use Cms\Classes\ComponentBase;

class OrderTracking extends ComponentBase
{

    public function componentDetails(){
        return [
            'name' => 'Order Tracking',
            'description' => 'History Tracking List of Order'
        ];
    }

    public function defineProperties()
    {
        return [
            'idElementTrackingDetails' => [
                'title'       => 'ID Element Tracking Details',
                'description' => 'This Element will be refresh after click Track My Order',
                'default'     => '#jkshop-tracking-details',
                'type'        => 'string'
            ],
            'idElementTotalCartPrice' => [
                'title'       => 'ID Element Total Cart Price',
                'description' => 'This Element will be refresh after ajax call - add product, etc..  (start with #)',
                'default'     => '#jkshop-total-basket',
                'type'        => 'string'
            ]
        ];
    }


    public function onTrackingOrder(){
        $trackingNumber = post("trackingNumber", null);
        $tracingResponse = $this->tracingItemJne($trackingNumber);
        $tracingResponse = json_decode($tracingResponse);
        if (isset($tracingResponse->error)) {
            $trackingResponseTiki =  $this->tracingItemTiki($trackingNumber);
            if ($trackingResponseTiki->response == null) {
                $data = array();
                $data["errorMessage"] = $tracingResponse->error;
                return [
                    $this->property("idElementTrackingDetails") => $this->renderPartial('::alert-message', $data)
                ];
            } else {
                $data = array();
                $data["history"] = $trackingResponseTiki->response[0]->history;
                return [
                    $this->property("idElementTrackingDetails") => $this->renderPartial('::tracking-details-tiki', $data)
                ];
            }
        } else {
            $data = array();
            $data["history"] = $tracingResponse->history;
            return [
                $this->property("idElementTrackingDetails") => $this->renderPartial('::tracking-details-jne', $data)
            ];
        }
        return;
    }

    public function tracingItemJne($trackingNumber) {
        $username = 'LESSENTIAL';
        $api_key = '791ff70496deadc7e1c96cd6e4571828';
//        $username = 'TESTAPI';
//        $api_key = '25c898a9faea1a100859ecd9ef674548';
//        $trackingNumberDev = 'CGKIT00002427416';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apiv2.jne.co.id:10101/tracing/api/list/cnote/$trackingNumber",
//            CURLOPT_URL => "http://apiv2.jne.co.id:10102/tracing/api/list/v1/cnote/$trackingNumber",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&api_key=$api_key",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "accept: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }

    public function getTikiAccessToken() {
        $username = 'ESSENTIAL';
        $password = 'ESSEN645';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id:8321/user/auth",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&password=$password",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        $token = $response->response->token;
        $err = curl_error($curl);

        curl_close($curl);
        return $token;
    }

    public function tracingItemTiki($trackingNumber) {
        $tikiAccessToken = $this->getTikiAccessToken();
        $cnno = $trackingNumber;
//        $cnnoDev = '030001547306';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id:8321/connote/mpds/history",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "cnno=$cnno",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "x-access-token: $tikiAccessToken"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }
}
