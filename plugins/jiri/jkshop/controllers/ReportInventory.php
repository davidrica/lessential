<?php namespace Jiri\JKShop\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Request;
use ApplicationException;
use DB;

/**
 * Report Inventory Back-end Controller
 */
class ReportInventory extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $requiredPermissions = ['jiri.jkshop.reportinventory'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Jiri.JKShop', 'jkshop', 'reportinventory');

        $this->vars['ordersCount'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->count();
        $this->vars['ordersPending'] = \Jiri\JKShop\Models\Order::where("paid_date", null)->where("created_at",">=", Carbon::now()->addDay(-1))->count();

        $this->vars['ordersCountThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->count();
        $this->vars['ordersCountLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay(-2), Carbon::now()->addDay(-1)])->count();
        $this->vars['ordersValueThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->sum("total_price");
        $this->vars['ordersValueLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay(-2), Carbon::now()->addDay(-1)])->sum("total_price");

        $this->vars['ordersCountClass'] = "positive"; if ($this->vars['ordersCountThis'] < $this->vars['ordersCountLast']) { $this->vars['ordersCountClass'] = "negative";  }
        $this->vars['ordersValueClass'] = "positive"; if ($this->vars['ordersValueThis'] < $this->vars['ordersValueLast']) { $this->vars['ordersValueClass'] = "negative";  }

        $this->addJs("https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js", "1");
        $this->addJs("/themes/lessential/assets/js/downloadPageAsPDF.js", "1");

        $this->getInventoryList();
    }

    function sortNumberOfOrders($a, $b) {
        return strcmp($a->id, $b->id);
    }

    function getProductTitleList($productIdList) {
        $productListDB = DB::table('jiri_jkshop_products')->get();
        $productTitleList = array();
        for ($i = 0; $i < count($productIdList); $i++) {
            for ($j = 0; $j < count($productListDB); $j++) {
                if ($productIdList[$i] == $productListDB[$j]->id) {
                    $productTitleList[$i] = $productListDB[$j]->title;
                }
            }
        }
        return $productTitleList;
    }

    public function getInventoryList() {
        $fromDate = Carbon::now()->addDay(-1)->format('Y-m-d');
        $toDate   = Carbon::now()->format('Y-m-d');
        $orderListDB = DB::table('jiri_jkshop_orders')->where("orderstatus_id", "=", "4")->where("shipping_name", "=", "JNE")->whereRaw("updated_at >= ? AND updated_at <= ?",
            array($fromDate." 15:00:00", $toDate." 14:59:59")
        )->get();

        // Create Order List object
        $orderDataList = array();
        for ($i = 0; $i < count($orderListDB); $i++) {
            $productQuantity = 0;
            $productWeight = 0;
            $productIdList = array();
            $productJson = json_decode($orderListDB[$i]->products_json, true);
            for ($j = 0; $j < count($productJson); $j++) {
                $productQuantity += $productJson[$j]['quantity'];
                $productWeight = $productJson[$j]['total_weight'];
                $productWeight = number_format($productWeight / 1000, 1);
                $productWeight = ceil($productWeight);
                $productIdList[$j] = $productJson[$j]['product_id'];
            }
            $productTitleList = $this->getProductTitleList($productIdList);
            $orderDataList[$i] = (object) array(
                'id' => $orderListDB[$i]->id,
                'reference_number' => $orderListDB[$i]->reference_number,
                'jne_airway_bill' => $orderListDB[$i]->jne_airway_bill,
                'ds_jne_code' => $orderListDB[$i]->ds_jne_code,
                'ds_tiki_code' => $orderListDB[$i]->ds_tiki_code,
                'quantity' => $productQuantity,
                'total_weight' => $productWeight,
                'product_title_list' => implode("<br> ",$productTitleList),
                'created_at' => $orderListDB[$i]->created_at,
            );
        }
        $this->vars['orderDataList'] = $orderDataList;
    }
}
