<?php namespace Jiri\JKShop\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Request;
use ApplicationException;
use DB;

/**
 * Statistics Back-end Controller
 */
class Statistics extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $requiredPermissions = ['jiri.jkshop.statistics'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Jiri.JKShop', 'jkshop', 'statistics');

        $this->vars['ordersCount'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->count();
        $this->vars['ordersPending'] = \Jiri\JKShop\Models\Order::where("paid_date", null)->where("created_at",">=", Carbon::now()->addDay(-1))->count();

        $this->vars['ordersCountThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->count();
        $this->vars['ordersCountLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay(-2), Carbon::now()->addDay(-1)])->count();
        $this->vars['ordersValueThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay(-1))->sum("total_price");
        $this->vars['ordersValueLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay(-2), Carbon::now()->addDay(-1)])->sum("total_price");

        $this->vars['ordersCountClass'] = "positive"; if ($this->vars['ordersCountThis'] < $this->vars['ordersCountLast']) { $this->vars['ordersCountClass'] = "negative";  }
        $this->vars['ordersValueClass'] = "positive"; if ($this->vars['ordersValueThis'] < $this->vars['ordersValueLast']) { $this->vars['ordersValueClass'] = "negative";  }

        $this->vars['orderTime'] = -1;
        $this->vars['day'] = 'today';

        $this->getBestSellingProducts();
        return [
            '#statistics' => $this->makePartial('statistics')
        ];
    }

    public function onChangeTime()
    {
        if (false) {
            throw new ApplicationException('Invalid value');
        }

        $this->vars['orderTime'] = post("orderTime");
        switch ($this->vars['orderTime']) {
            case '-1':
                $this->vars['day'] = 'today';
                break;
            case '-2':
                $this->vars['day'] = 'yesterday';
                break;
            case '-7':
                $this->vars['day'] = '7 DAYS';
                break;
            case '-30':
                $this->vars['day'] = '30 DAYS';
                break;
        }

        $this->vars['ordersCount'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay($this->vars['orderTime']))->count();
        $this->vars['ordersPending'] = \Jiri\JKShop\Models\Order::where("paid_date", null)->where("created_at",">=", Carbon::now()->addDay($this->vars['orderTime']))->count();


        $this->vars['ordersCountThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay($this->vars['orderTime']))->count();
        $this->vars['ordersCountLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay($this->vars['orderTime']* 2), Carbon::now()->addDay($this->vars['orderTime'])])->count();
        $this->vars['ordersValueThis'] = \Jiri\JKShop\Models\Order::where("created_at",">=", Carbon::now()->addDay($this->vars['orderTime']))->sum("total_price");
        $this->vars['ordersValueLast'] = \Jiri\JKShop\Models\Order::whereBetween("created_at", [Carbon::now()->addDay($this->vars['orderTime']* 2), Carbon::now()->addDay($this->vars['orderTime'])])->sum("total_price");

        $this->vars['ordersCountClass'] = "positive"; if ($this->vars['ordersCountThis'] < $this->vars['ordersCountLast']) { $this->vars['ordersCountClass'] = "negative";  }
        $this->vars['ordersValueClass'] = "positive"; if ($this->vars['ordersValueThis'] < $this->vars['ordersValueLast']) { $this->vars['ordersValueClass'] = "negative";  }

        return [
            '#statistics' => $this->makePartial('statistics')
        ];
    }

    function sortNumberOfOrders($a, $b) {
        return strcmp($a->id, $b->id);
    }

    public function getBestSellingProducts() {
//        $productListDB = DB::table('jiri_jkshop_products')->get();
        $productList = \Jiri\JKShop\Models\Product::get();
        $productIdList = $productList->pluck('id');
//        $productTitleList = $productList->pluck('title');
        $orderList = \Jiri\JKShop\Models\Order::get();
        $orderEmailList = \Jiri\JKShop\Models\Order::get()->pluck('contact_email');
        $orderProductList = \Jiri\JKShop\Models\Order::get()->pluck('products_json');
        $orderByCityList = \Jiri\JKShop\Models\Order::get()->pluck('ds_city');

        // Get Product Rank
        $orderProductIdList = array();
        foreach ($orderProductList as $order) {
            foreach ($order as $item) {
                array_push($orderProductIdList, $item['product_id']);
            }
        }

        $segmentedProductIdList = array();
        foreach ($productIdList as $productId) {
            $filteredId = array();
            foreach ($orderProductIdList as $orderProductId) {
                if ($orderProductId == $productId) {
                    array_push($filteredId, $orderProductId);
                }
            }
            array_push($segmentedProductIdList, count($filteredId));
        }

        $ProductRankList = array();
        for ($i = 0; $i < count($productList); $i++) {
            $ProductRankList[$i] = (object) array(
                'id' => $productList[$i]['id'],
                'title' => $productList[$i]['title'],
                'default_category_id' => $productList[$i]['default_category_id'],
                'retail_price_with_tax' => $productList[$i]['retail_price_with_tax'],
                'number_of_orders' => $segmentedProductIdList[$i],
            );
        }

        usort($ProductRankList,function($first,$second){
            return $first->number_of_orders < $second->number_of_orders;
        });

        $this->vars['productRankList'] = $ProductRankList;

        // Get City List
        $cityList = array();
        foreach ($orderByCityList as $orderByCity) {
            array_push($cityList, $orderByCity);
        }
        $cityList = array_unique($cityList);

        $segmentedOrderByCityList = array();
        foreach ($cityList as $key => $city) {
            $filteredCity = array();
            foreach ($orderByCityList as $orderByCity) {
                if ($orderByCity == $city) {
                    array_push($filteredCity, $orderByCity);
                }
            }
            $segmentedOrderByCityList[$key] = (object) array(
                'city' => $city,
                'value' => count($filteredCity)
            );
        }
        usort($segmentedOrderByCityList,function($first,$second){
            return $first->value < $second->value;
        });
        $this->vars['segmentedOrderByCityList'] = $segmentedOrderByCityList;


        // Get Email Rank
        $emailList = array();
        foreach ($orderEmailList as $orderEmail) {
            array_push($emailList, $orderEmail);
        }
        $emailList = array_values(array_unique($emailList));

        $segmentedEmailList = array();
        foreach ($emailList as $email) {
            $filteredEmail = array();
            foreach ($orderEmailList as $orderEmail) {
                if ($orderEmail == $email) {
                    array_push($filteredEmail, $orderEmail);
                }
            }
            array_push($segmentedEmailList, count($filteredEmail));
        }

        $emailRankList = array();
        for ($i = 0; $i < count($emailList); $i++) {
            $emailRankList[$i] = (object) array(
                'email' => $emailList[$i],
                'number_of_orders' => $segmentedEmailList[$i]
            );
        }
        usort($emailRankList,function($first,$second){
            return $first->number_of_orders < $second->number_of_orders;
        });
        $this->vars['emailRankList'] = $emailRankList;

        usort($ProductRankList,function($first,$second){
            return $first->number_of_orders < $second->number_of_orders;
        });

        $this->vars['productRankList'] = $ProductRankList;

//        dd($segmentedOrderByCityList);
    }
}
