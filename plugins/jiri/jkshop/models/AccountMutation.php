<?php namespace Jiri\JKShop\Models;

use DB;
use Mail;
use Model;

/**
 * Account Mutation Model
 */
class AccountMutation extends Model
{
    public function getAccessToken() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.klikbca.com/api/oauth/token",
//            CURLOPT_URL => "https://devapi.klikbca.com/api/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "authorization: Basic YTYyZThlMzgtNzc5Yi00ZjhlLTk4MTEtZWY1YzU0YmM1MmYzOmViMjJmMmNlLTJmOGMtNDU2Ni1iZGFiLWEwODVjNjM2MGY3ZQ=="
//                "authorization: Basic YjA5NWFjOWQtMmQyMS00MmEzLWE3MGMtNDc4MWY0NTcwNzA0OmJlZGQxZjhkLTNiZDYtNGQ0YS04Y2I0LWU2MWRiNDE2OTFjOQ=="
            ),
        ));

        $response = curl_exec($curl);
//        dd($response);
        curl_close($curl);
        $accessToken = json_decode($response)->access_token;
        $this->getSignature($accessToken);
    }

    public function getSignature($accessToken) {
        date_default_timezone_set("Etc/GMT-7");
        $timestamp = date('Y-m-d\TH:i:s\.000+07.00');
        $startDate = date('Y-m-d', strtotime('-2 days'));
        $endDate = date('Y-m-d', strtotime('0 days'));
        $string = "GET:/banking/v3/corporates/IBSLESSENT/accounts/4977998998/statements?EndDate=$endDate&StartDate=$startDate:$accessToken:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:$timestamp";
        $secret = '5847870e-420a-4494-9d6d-881116462284';
//        $string = "GET:/banking/v3/corporates/h2hauto008/accounts/0611105893/statements?EndDate=$endDate&StartDate=$startDate:$accessToken:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:$timestamp";
//        $secret = '5e636b16-df7f-4a53-afbe-497e6fe07edc';
        $signature = hash_hmac('sha256', $string, $secret);
        $this->getMutation($accessToken, $signature, $startDate, $endDate, $timestamp);
    }

    public function getMutation($accessToken, $signature, $startDate, $endDate, $timestamp) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.klikbca.com:443/banking/v3/corporates/IBSLESSENT/accounts/4977998998/statements?StartDate=$startDate&EndDate=$endDate",
//            CURLOPT_URL => "https://devapi.klikbca.com:443/banking/v3/corporates/h2hauto008/accounts/0611105893/statements?StartDate=$startDate&EndDate=$endDate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "authorization: Bearer $accessToken",
                "x-bca-key: 52637cc5-a7bf-49bb-9a66-e5897ee4664f",
//                "x-bca-key: dcc99ba6-3b2f-479b-9f85-86a09ccaaacf",
                "x-bca-signature: $signature",
                "x-bca-timestamp: $timestamp"
            ),
        ));

        $response = curl_exec($curl);
//        dd($response);
        curl_close($curl);
        if (isset(json_decode($response)->Data)) {
            $transactionList = json_decode($response)->Data;
            $this->updateOrderStatus($transactionList, $startDate);
        }
    }

    public function updateOrderStatus($transactionList, $startDate) {
        $paidOrderList = array();
        foreach ($transactionList as $transaction) {
            $order = DB::table('jiri_jkshop_orders')
                ->whereDate('created_at', '>=', $startDate)
                ->where('orderstatus_id', 3)
                ->where('total_price', substr($transaction->TransactionAmount, 0, -3));
            if ($order->first()) {
                array_push($paidOrderList, $order->first());
            }
            $order->update(['orderstatus_id' => 4]);
        }
        foreach ($paidOrderList as $paidOrder) {
            $this->sendMail($paidOrder->id, $paidOrder->updated_at, $paidOrder->ds_city, $paidOrder->total_price);
        }
    }

    public function sendMail($id, $updated_at, $ds_city, $total_price) {
        $vars = ['order_id' => "$id", 'time' => "$updated_at",  'subject' => "Payment Received #$id", 'total_price' => "$total_price", 'ds_city' => "$ds_city"];
        Mail::send('jiri.jkshop::mail.payment-received', $vars, function($message) {

            $message->from('order@lessentialbeauty.id', 'lessentialbeauty.id');
            $message->to('davidlikaldo@gmail.com', 'Lessential Beauty');
            $message->subject('Payment received');

        });
    }
}
