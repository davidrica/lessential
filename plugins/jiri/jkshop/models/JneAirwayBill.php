<?php namespace Jiri\JKShop\Models;

use DB;
use Carbon\Carbon;

class JneAirwayBill
{
    public function generateCNote() {
        $orderData = $this->getDataFields();
        $orderId = $orderData->reference_number;
        $shipperName = 'Lessential Beauty';
        $shipperAddress1 = 'Jalan Pergudangan Industri Taman Tekno BSD, Jl. Sektor 11 No.3A, Setu, Kec. Setu';
        $shipperAddress2 = '';
        $shipperAddress3 = '';
        $shipperCity = 'Tanggerang';
        $shipperRegion = 'Banten';
        $shipperZip = '15314';
        $shipperPhone = '(021) 75881990';
        $receiverName = DB::table('users')->where("email","=", $orderData->contact_email)->get()[0]->name;
        $receiverAddress1 = $orderData->address;
        $receiverAddress2 = '';
        $receiverAddress3 = '';
        $receiverCity = $orderData->city;
        $receiverRegion = $orderData->region;
        $receiverZip = $orderData->postcode;
        $receiverPhone = $orderData->contact_phone;
        $qty = $orderData->quantity;
        $weight = $orderData->total_weight;
        $goodsDesc = $orderData->product_title_list;
        $goodsValue = $orderData->goods_value;
        $dest = $orderData->ds_jne_code;
        $service = $orderData->shipping_service;

        $curl = curl_init();
        $fields = array(
            'username' => 'LESSENTIAL',
            'api_key' => '791ff70496deadc7e1c96cd6e4571828',
            'OLSHOP_BRANCH' => 'TGR000',
            'OLSHOP_CUST' => '11096500',
            'OLSHOP_ORDERID' => $orderId,
            'OLSHOP_SHIPPER_NAME' => $shipperName,
            'OLSHOP_SHIPPER_ADDR1' => $shipperAddress1,
            'OLSHOP_SHIPPER_ADDR2' => $shipperAddress2,
            'OLSHOP_SHIPPER_ADDR3' => $shipperAddress3,
            'OLSHOP_SHIPPER_CITY' => $shipperCity,
            'OLSHOP_SHIPPER_REGION' => $shipperRegion,
            'OLSHOP_SHIPPER_ZIP' => $shipperZip,
            'OLSHOP_SHIPPER_PHONE' => $shipperPhone,
            'OLSHOP_RECEIVER_NAME' => $receiverName,
            'OLSHOP_RECEIVER_ADDR1' => $receiverAddress1,
            'OLSHOP_RECEIVER_ADDR2' => $receiverAddress2,
            'OLSHOP_RECEIVER_ADDR3' => $receiverAddress3,
            'OLSHOP_RECEIVER_CITY' => $receiverCity,
            'OLSHOP_RECEIVER_REGION' => $receiverRegion,
            'OLSHOP_RECEIVER_ZIP' => $receiverZip,
            'OLSHOP_RECEIVER_PHONE' => $receiverPhone,
            'OLSHOP_QTY' => $qty,
            'OLSHOP_WEIGHT' => $weight,
            'OLSHOP_GOODSDESC' => $goodsDesc,
            'OLSHOP_GOODSVALUE' => $goodsValue,
            'OLSHOP_GOODSTYPE' => '2',
            'OLSHOP_INS_FLAG' => 'N',
            'OLSHOP_ORIG' => 'TGR10000',
            'OLSHOP_DEST' => $dest,
            'OLSHOP_SERVICE' => $service,
            'OLSHOP_COD_FLAG' => 'N',
            'OLSHOP_COD_AMOUNT' => '0',
        );

        $postVars = '';
        foreach($fields as $key=>$value) {
            $postVars .= $key . "=" . $value . "&";
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apiv2.jne.co.id:10101/tracing/api/generatecnote",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "$postVars",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response = json_decode($response);

        curl_close($curl);
        DB::table('jiri_jkshop_orders')->where("id","=", $orderData->id)->update(['jne_airway_bill' => $response->detail[0]->cnote_no]);
    }

    function getDataFields() {
        $orderListDB = DB::table('jiri_jkshop_orders')->where("updated_at",">", Carbon::now()->addDay(-1))->get();

        // Create Order List object
        $orderDataList = array();
        for ($i = 0; $i < count($orderListDB); $i++) {
            $productQuantity = 0;
            $productWeight = 0;
            $productIdList = array();
            $productJson = json_decode($orderListDB[$i]->products_json, true);
            for ($j = 0; $j < count($productJson); $j++) {
                $productQuantity += $productJson[$j]['quantity'];
                $productWeight = $productJson[$j]['total_weight'];
                $productWeight = number_format($productWeight / 1000, 1);
                $productWeight = ceil($productWeight);
                $productIdList[$j] = $productJson[$j]['product_id'];
            }
            $productTitleList = $this->getProductTitleList($productIdList);
            $orderDataList[$i] = (object) array(
                'id' => $orderListDB[$i]->id,
                'reference_number' => $orderListDB[$i]->reference_number,
                'contact_email' => $orderListDB[$i]->contact_email,
                'address' => $orderListDB[$i]->ds_address.', '.$orderListDB[$i]->ds_kecamatan,
                'city' => $orderListDB[$i]->ds_city,
                'region' => $orderListDB[$i]->ds_county,
                'postcode' => $orderListDB[$i]->ds_postcode,
                'contact_phone' => $orderListDB[$i]->contact_phone,
                'quantity' => $productQuantity,
                'total_weight' => $productWeight,
                'product_title_list' => implode("",$productTitleList),
                'goods_value' => $orderListDB[$i]->total_price - $orderListDB[$i]->shipping_price,
                'ds_jne_code' => $orderListDB[$i]->ds_jne_code,
                'created_at' => $orderListDB[$i]->created_at,
                'shipping_service' => $orderListDB[$i]->shipping_service
            );
        }
        $orderData = $orderDataList[count($orderDataList)-1];
        return $orderData;
    }

    function getProductTitleList($productIdList) {
        $productListDB = DB::table('jiri_jkshop_products')->get();
        $productTitleList = array();
        for ($i = 0; $i < count($productIdList); $i++) {
            for ($j = 0; $j < count($productListDB); $j++) {
                if ($productIdList[$i] === $productListDB[$j]->id) {
                    $productTitleList[$i] = $productListDB[$j]->title;
                }
            }
        }
        return $productTitleList;
    }

    public function getDestinationList() {
        $jneDestinationList = DB::table('jne_destinations')->get();
        return $jneDestinationList;
    }

    public function getProvinceList() {
        $jneProvinceList = DB::table('jne_destinations')->select('provinsi')->groupBy('provinsi')->get();
        return $jneProvinceList;
    }

    public function getCityList() {
        $jneCityList = DB::table('jne_destinations')->select('provinsi', 'kota')->groupBy('kota')->get();
        return $jneCityList;
    }

    public function getKecamatanList() {
        $jneKecamatanList = DB::table('jne_destinations')->select('kota', 'kecamatan', 'destination_code')->groupBy('kecamatan')->get();
        return $jneKecamatanList;
    }

    public function getShippingCostList() {
        $username = 'LESSENTIAL';
        $api_key = '791ff70496deadc7e1c96cd6e4571828';
        $from = 'TGR10000';
        $thru = post("thru", null);
        $weight = post("weight", 1);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apiv2.jne.co.id:10101/tracing/api/pricedev",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&api_key=$api_key&from=$from&thru=$thru&weight=$weight",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "accept: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }
}
