<?php namespace Jiri\JKShop\Models;

use DB;
use Mail;
use Model;

/**
 * Online Transfer Model
 */
class OnlineTransfer extends Model
{
    public function getAccessToken() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://devapi.klikbca.com/api/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "authorization: Basic YjA5NWFjOWQtMmQyMS00MmEzLWE3MGMtNDc4MWY0NTcwNzA0OmJlZGQxZjhkLTNiZDYtNGQ0YS04Y2I0LWU2MWRiNDE2OTFjOQ=="
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $accessToken = json_decode($response)->access_token;
        $this->getDataTransfer($accessToken);
        $this->transfer();
    }

    public function getDataTransfer($accessToken) {
        date_default_timezone_set("Etc/GMT-7");
        $timestamp = date('Y-m-d\TH:i:s\.000+07.00');

        $amount = "1500000.00";
        $BeneficiaryAccountNumber = "0611105893";
        $CorporateID = "h2hauto008";
        $CurrencyCode = "IDR";
        $ReferenceID = date('YmdHis');
        $Remark1 = "Expired";
        $Remark2 = "AccessToken";
        $SourceAccountNumber = "0613005827";
        $TransactionDate = date('Y-m-d');
        $TransactionID = "00000054";

        $data = array(
            "Amount" => $amount,
            "BeneficiaryAccountNumber" => $BeneficiaryAccountNumber,
            "CorporateID" => $CorporateID,
            "CurrencyCode" => $CurrencyCode,
            "ReferenceID" => $ReferenceID,
            "Remark1" => $Remark1,
            "Remark2" => $Remark2,
            "SourceAccountNumber" => $SourceAccountNumber,
            "TransactionDate" => $TransactionDate,
            "TransactionID" => $TransactionID,
        );
        $canonicalizedBody = json_encode($data);
        $ExamineRequestBody = hash( 'sha256' , $canonicalizedBody, false);
        $this->getSignature($accessToken, $canonicalizedBody, $ExamineRequestBody, $amount, $BeneficiaryAccountNumber, $CorporateID,
            $CurrencyCode, $ReferenceID, $Remark1, $Remark2, $SourceAccountNumber, $TransactionDate, $TransactionID, $timestamp);
    }

    public function getSignature($accessToken, $canonicalizedBody, $ExamineRequestBody, $amount, $BeneficiaryAccountNumber, $CorporateID,
                                 $CurrencyCode, $ReferenceID, $Remark1, $Remark2, $SourceAccountNumber, $TransactionDate,
                                 $TransactionID, $timestamp) {
        $string = "POST:/banking/corporates/transfers:$accessToken:$ExamineRequestBody:$timestamp";
        $secret = '5e636b16-df7f-4a53-afbe-497e6fe07edc';
        $signature = hash_hmac('sha256', $string, $secret);
        $this->transfer($accessToken, $canonicalizedBody, $signature, $timestamp, $amount, $BeneficiaryAccountNumber, $CorporateID,
            $CurrencyCode, $ReferenceID, $Remark1, $Remark2, $SourceAccountNumber, $TransactionDate, $TransactionID);
    }

    public function transfer($accessToken, $canonicalizedBody, $signature, $timestamp) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://devapi.klikbca.com:443/banking/corporates/transfers",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $canonicalizedBody,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "authorization: Bearer $accessToken",
                "x-bca-key: dcc99ba6-3b2f-479b-9f85-86a09ccaaacf",
                "x-bca-signature: $signature",
                "x-bca-timestamp: $timestamp"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $transactionDetail = json_decode($response);
//        dd($accessToken, $signature, $timestamp, $canonicalizedBody, $response);
//        $this->updateOrderStatus($transactionList, $startDate);
    }

    public function updateOrderStatus($transactionList, $startDate) {
        $paidOrderList = array();
        foreach ($transactionList as $transaction) {
            $order = DB::table('jiri_jkshop_orders')
                ->whereDate('created_at', '>', $startDate)
                ->where('orderstatus_id', 3)
                ->where('total_price', substr($transaction->TransactionAmount, 0, -3));
            if ($order->first()) {
                array_push($paidOrderList, $order->first());
            }
            $order->update(['orderstatus_id' => 4]);
        }
        foreach ($paidOrderList as $paidOrder) {
            $this->sendMail($paidOrder->id, $paidOrder->updated_at, $paidOrder->ds_city, $paidOrder->total_price);
        }
    }

    public function sendMail($id, $updated_at, $ds_city, $total_price) {
        $vars = ['order_id' => "$id", 'time' => "$updated_at",  'subject' => "Payment Received #$id", 'total_price' => "$total_price", 'ds_city' => "$ds_city"];
        Mail::send('jiri.jkshop::mail.payment-received', $vars, function($message) {

            $message->from('order@lessentialbeauty.id', 'lessentialbeauty.id');
            $message->to('davidlikaldo@gmail.com', 'Lessential Beauty');
            $message->subject('Payment received');

        });
    }
}
