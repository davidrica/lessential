<?php namespace Jiri\JKShop\Models;

use DB;

/**
 * Order Model
 */
class Shipping
{
    // JNE
    public function getDestinationList() {
        $jneDestinationList = DB::table('jne_destinations')->get();
        return $jneDestinationList;
    }

    public function getProvinceList() {
        $jneProvinceList = DB::table('jne_destinations')->select('provinsi')->groupBy('provinsi')->get();
        return $jneProvinceList;
    }

    public function getCityList() {
        $jneCityList = DB::table('jne_destinations')->select('provinsi', 'kota')->groupBy('kota')->get();
        return $jneCityList;
    }

    public function getKecamatanList() {
        $jneKecamatanList = DB::table('jne_destinations')->select('kota', 'kecamatan', 'destination_code', 'kode_pos')->groupBy('kecamatan')->get();
        return $jneKecamatanList;
    }

    public function getShippingCostList() {
        $username = 'LESSENTIAL';
        $api_key = '791ff70496deadc7e1c96cd6e4571828';
        $from = 'TGR10000';
        $thru = post("thru", null);
        $weight = post("weight", 1);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apiv2.jne.co.id:10101/tracing/api/pricedev",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&api_key=$api_key&from=$from&thru=$thru&weight=$weight",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "accept: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }
    // JNE End

    // Tiki
    public function getTikiAccessToken() {
        $username = '6871759475819407393';
        $password = 'cf0fe810918e76c9b38bfca6fbab0a3b66961acf';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id.net:80/user/auth",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&password=$password",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        $token = $response->response->token;
        $err = curl_error($curl);

        curl_close($curl);
        return $token;
    }

    public function getTikiAreaInfo() {
        $tikiAreaInfo = file_get_contents(base_path('json/shipping/tiki_area_info.json'));
        $data = json_decode($tikiAreaInfo, true);
        return $data;
    }

    public function getTikiShippingCostList() {
        $token = post("token", null);
        $orig = post("orig", null);
        $dest = post("dest", null);
        $weight = post("weight", 1);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id.net:80/tariff/product",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "orig=$orig&dest=$dest&weight=$weight",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "x-access-token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }
}
