<?php namespace Jiri\JKShop\Models;

use DB;
use Carbon\Carbon;

class TikiAirwayBill
{
    public function generateCNote() {
        $tikiAccessToken = $this->getTikiAccessToken();
        $orderData = $this->getDataFields();

        $accnum = 'TGR010594001';
        $paket_awb = '';
        $paket_id = $orderData->tiki_reference_number;
        $paket_content = $orderData->product_title_list;
        $paket_value = $orderData->goods_value;
        $paket_cod = '0';
        $paket_service = $orderData->shipping_service;
        $consignor_name = 'Lessential Beauty';
        $consignor_company = '';
        $consignor_address1 = 'Jalan Pergudangan Industri Taman Tekno BSD, Jl. Sektor 11 No.3A, Setu, Kec. Setu';
        $consignor_address2 = 'Tanggerang';
        $consignor_zipcode = '15314';
        $consignor_phone = '(021) 75881990';
        $consignee_name = DB::table('users')->where("email","=", $orderData->contact_email)->get()[0]->name;
        $consignee_company = '';
        $consignee_address1 = $orderData->address;
        $consignee_address2 = $orderData->city;
        $consignee_zipcode = $orderData->postcode;
        $consignee_phone = $orderData->contact_phone;




        $curl = curl_init();
        $fields = array(
            'accnum' => $accnum,
            'paket_awb' => $paket_awb,
            'paket_id' => $paket_id,
            'paket_content' => $paket_content,
            'paket_value' => $paket_value,
            'paket_cod' => $paket_cod,
            'paket_service' => $paket_service,
            'consignor_name' => $consignor_name,
            'consignor_company' => $consignor_company,
            'consignor_address1' => $consignor_address1,
            'consignor_address2' => $consignor_address2,
            'consignor_zipcode' => $consignor_zipcode,
            'consignor_phone' => $consignor_phone,
            'consignee_name' => $consignee_name,
            'consignee_company' => $consignee_company,
            'consignee_address1' => $consignee_address1,
            'consignee_address2' => $consignee_address2,
            'consignee_zipcode' => $consignee_zipcode,
            'consignee_phone' => $consignee_phone
        );

        $postVars = '';
        foreach($fields as $key=>$value) {
            $postVars .= $key . "=" . $value . "&";
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id:80/mde/manifestorder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "$postVars",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "x-access-token: $tikiAccessToken"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response = json_decode($response);

        curl_close($curl);
        DB::table('jiri_jkshop_orders')->where("id","=", $orderData->id)->update(['tiki_airway_bill' => $response->response->paket_id]);
    }

    public function getTikiAccessToken() {
        $username = '6871759475819407393';
        $password = 'cf0fe810918e76c9b38bfca6fbab0a3b66961acf';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apix.tikiku.id:80/user/auth",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&password=$password",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        $token = $response->response->token;
        $err = curl_error($curl);

        curl_close($curl);
        return $token;
    }

    function getDataFields() {
        $orderListDB = DB::table('jiri_jkshop_orders')->where("updated_at",">", Carbon::now()->addDay(-1))->get();

        // Create Order List object
        $orderDataList = array();
        for ($i = 0; $i < count($orderListDB); $i++) {
            $productQuantity = 0;
            $productWeight = 0;
            $productIdList = array();
            $productJson = json_decode($orderListDB[$i]->products_json, true);
            for ($j = 0; $j < count($productJson); $j++) {
                $productQuantity += $productJson[$j]['quantity'];
                $productWeight = $productJson[$j]['total_weight'];
                $productWeight = number_format($productWeight / 1000, 1);
                $productWeight = ceil($productWeight);
                $productIdList[$j] = $productJson[$j]['product_id'];
            }
            $productTitleList = $this->getProductTitleList($productIdList);
            $orderDataList[$i] = (object) array(
                'id' => $orderListDB[$i]->id,
                'reference_number' => $orderListDB[$i]->reference_number,
                'tiki_reference_number' => $orderListDB[$i]->tiki_reference_number,
                'contact_email' => $orderListDB[$i]->contact_email,
                'address' => $orderListDB[$i]->ds_address.', '.$orderListDB[$i]->ds_kecamatan,
                'city' => $orderListDB[$i]->ds_city,
                'region' => $orderListDB[$i]->ds_county,
                'postcode' => $orderListDB[$i]->ds_postcode,
                'contact_phone' => $orderListDB[$i]->contact_phone,
                'quantity' => $productQuantity,
                'total_weight' => $productWeight,
                'product_title_list' => implode("",$productTitleList),
                'goods_value' => $orderListDB[$i]->total_price - $orderListDB[$i]->shipping_price,
                'ds_jne_code' => $orderListDB[$i]->ds_jne_code,
                'created_at' => $orderListDB[$i]->created_at,
                'shipping_service' => $orderListDB[$i]->shipping_service
            );
        }
        $orderData = $orderDataList[count($orderDataList)-1];
        return $orderData;
    }

    function getProductTitleList($productIdList) {
        $productListDB = DB::table('jiri_jkshop_products')->get();
        $productTitleList = array();
        for ($i = 0; $i < count($productIdList); $i++) {
            for ($j = 0; $j < count($productListDB); $j++) {
                if ($productIdList[$i] == $productListDB[$j]->id) {
                    $productTitleList[$i] = $productListDB[$j]->title;
                }
            }
        }
        return $productTitleList;
    }

    public function getDestinationList() {
        $jneDestinationList = DB::table('jne_destinations')->get();
        return $jneDestinationList;
    }

    public function getProvinceList() {
        $jneProvinceList = DB::table('jne_destinations')->select('provinsi')->groupBy('provinsi')->get();
        return $jneProvinceList;
    }

    public function getCityList() {
        $jneCityList = DB::table('jne_destinations')->select('provinsi', 'kota')->groupBy('kota')->get();
        return $jneCityList;
    }

    public function getKecamatanList() {
        $jneKecamatanList = DB::table('jne_destinations')->select('kota', 'kecamatan', 'destination_code')->groupBy('kecamatan')->get();
        return $jneKecamatanList;
    }

    public function getShippingCostList() {
        $username = 'LESSENTIAL';
        $api_key = '791ff70496deadc7e1c96cd6e4571828';
        $from = 'TGR10000';
        $thru = post("thru", null);
        $weight = post("weight", 1);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://apiv2.jne.co.id:10101/tracing/api/pricedev",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=$username&api_key=$api_key&from=$from&thru=$thru&weight=$weight",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "accept: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
    }
}
