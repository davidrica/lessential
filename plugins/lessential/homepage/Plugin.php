<?php namespace Lessential\Homepage;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerNavigation() {
        return [
            'homepage' => [
                'label' => 'Homepage',
                'url' => Backend::url('lessential/homepage/sliders'),
                'icon' => 'icon-home',
                'permissions' => ['lessential.homepage.*'],
                'order' => 69,
                'sideMenu' => [
                    'sliders' => [
                        'label' => 'Sliders',
                        'icon' => 'icon-desktop',
                        'url' => Backend::url('lessential/homepage/sliders'),
                        'permissions' => ['lessential.homepage.sliders'],
                    ],
                    'testimonials' => [
                        'label' => 'Testimonials',
                        'icon' => 'icon-users',
                        'url' => Backend::url('lessential/homepage/testimonials'),
                        'permissions' => ['lessential.homepage.testimonials'],
                    ]
                ]
            ]
        ];
    }

    public function registerSettings()
    {
    }
}
