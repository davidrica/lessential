<?php namespace Lessential\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLessentialHomepageSliders extends Migration
{
    public function up()
    {
        Schema::create('lessential_homepage_sliders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lessential_homepage_sliders');
    }
}
