<?php namespace Lessential\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLessentialHomepageTestimonials extends Migration
{
    public function up()
    {
        Schema::create('lessential_homepage_testimonials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->smallInteger('testimonial');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lessential_homepage_testimonials');
    }
}
