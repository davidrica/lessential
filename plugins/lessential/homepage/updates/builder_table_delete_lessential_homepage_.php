<?php namespace Lessential\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteLessentialHomepage extends Migration
{
    public function up()
    {
        Schema::dropIfExists('lessential_homepage_');
    }
    
    public function down()
    {
        Schema::create('lessential_homepage_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
}
