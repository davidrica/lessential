<?php namespace Lessential\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLessentialHomepageTestimonials extends Migration
{
    public function up()
    {
        Schema::table('lessential_homepage_testimonials', function($table)
        {
            $table->text('testimonial')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('lessential_homepage_testimonials', function($table)
        {
            $table->smallInteger('testimonial')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
