/* Download Inventory List as PDF */
var doc = new jsPDF({
    orientation: 'landscape',
    unit: 'px',
    format: 'a1'
});
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

const downloadInventory = () => {
    console.log('o');
    doc.setFontSize(100);
    doc.fromHTML($('#reportinventory').html(), 15, 5, {
        'width': 170,
        'elementHandlers': specialElementHandlers,
        'margin': {left: 900, right: 900}
    });
    doc.save('inventory-list.pdf');
};
/* End */
